﻿using System;
using System.Collections.Generic;

namespace VCPU_16
{
    /// <summary>
    /// Defines all the instructions
    /// </summary>
    public enum InstructionType
    {
        Nop = 0,
        Brk = 1,
        Hlt = 2,
        Err = 3,
        Mov = 4,
        Add = 5,
        Sub = 6,
        Jmp = 7,
        Cmp = 8,
        Je  = 9,
        Drf = 10,
        Ptr = 11
    }

    /// <summary>
    /// Defines all the combinations of argument location encoding
    /// </summary>
    public enum ArgumentLocationEncodingType
    {
        RegisterAndMemory = 0,
        RegisterAndRegister = 1,
        RegisterAndConst = 2,
        MemoryAndMemory = 3,
        MemoryAndRegister = 4,
        MemoryAndConstant = 5
    }

    internal class Instruction
    {
        private const int OpCodeMask = 255;
        private const int LocationEncodingMask = 3840;
        private const int LocationEncodingShift = 8;
        private const int SingleRegisterMask = 61440;
        private const int SingleRegisterShift = 12;
        private const int FirstRegisterMask = 61440;
        private const int FirstRegisterShift = 8;
        private const int SecondRegisterMask = 3840;

        public List<Token> Arguments = new List<Token>();
        public InstructionType Type;

        /// <summary>
        /// This method returns the binary representation of the encoding argument location encoding of this instruction.
        /// This tells the CPU what kind of arguments to expect
        /// </summary>
        /// <param name="arguments">List of all the arguments for this opcode</param>
        /// <param name="opcodeNumber">Number of the opcode as defined in InstructionType</param>
        /// <returns>Argument location encoding of this instruction</returns>
        private static ushort[] EncodeArgumentLocationEncoding(List<Token> arguments, ushort opcodeNumber)
        {
            var opCode = new List<ushort>();

            arguments[0].Data = ushort.Parse((string) arguments[0].Data);
            arguments[1].Data = ushort.Parse((string) arguments[1].Data);

            switch (arguments[0].Type)
            {
                case TokenType.Register:
                    switch (arguments[1].Type)
                    {
                        case TokenType.Register:
                            opCode.Add((ushort) (((int)ArgumentLocationEncodingType.RegisterAndRegister << LocationEncodingShift) | opcodeNumber));
                            opCode.Add((ushort) ((ushort) ((ushort) arguments[0].Data << FirstRegisterShift) | (ushort) arguments[1].Data));
                            break;
                        case TokenType.Number:
                            opCode.Add((ushort) ((ushort) (((ushort) arguments[0].Data << SingleRegisterShift) | ((int)ArgumentLocationEncodingType.RegisterAndConst << LocationEncodingShift)) | opcodeNumber));
                            opCode.Add((ushort) arguments[1].Data);
                            break;
                        case TokenType.MemoryLocation:
                            opCode.Add((ushort) ((ushort) ((ushort) arguments[0].Data << SingleRegisterShift) | opcodeNumber));
                            opCode.Add((ushort) arguments[1].Data);
                            break;
                        case TokenType.Label:
                            opCode.Add((ushort) arguments[0].Data);
                            break;
                    }

                    break;
                case TokenType.MemoryLocation:
                    switch (arguments[1].Type)
                    {
                        case TokenType.Register:
                            opCode.Add((ushort) ((ushort) (((ushort) arguments[1].Data << SingleRegisterShift) | ((int)ArgumentLocationEncodingType.MemoryAndRegister << LocationEncodingShift)) | opcodeNumber));
                            opCode.Add((ushort) arguments[0].Data);
                            break;
                        case TokenType.Number:
                            opCode.Add((ushort) (((int)ArgumentLocationEncodingType.MemoryAndConstant << LocationEncodingShift) | opcodeNumber));
                            opCode.Add((ushort) arguments[0].Data);
                            opCode.Add((ushort) arguments[1].Data);
                            break;
                        case TokenType.MemoryLocation:
                            opCode.Add((ushort) (((int)ArgumentLocationEncodingType.MemoryAndMemory << LocationEncodingShift) | opcodeNumber));
                            opCode.Add((ushort) arguments[0].Data);
                            opCode.Add((ushort) arguments[1].Data);
                            break;
                    }

                    break;
            }
            return opCode.ToArray();
        }

        /// <summary>
        /// Returns the binary represantation of this instruction.
        /// </summary>
        /// <returns>Binary representation of this instruction.</returns>
        public ushort[] AssembleByteCode()
        {
            switch (Type)
            {
                case InstructionType.Nop:
                    return new[] { (ushort)InstructionType.Nop };
                case InstructionType.Brk:
                    return new[] { (ushort)InstructionType.Brk };
                case InstructionType.Hlt:
                    return new[] { (ushort)InstructionType.Hlt };
                case InstructionType.Err:
                    return new[] { (ushort)InstructionType.Err };
                case InstructionType.Mov:
                    if (Arguments.Count != 2)
                        throw new Exception("Invalid amount of arguments for opcode MOV. 2 expected, got " +
                                            Arguments.Count);
                    if (Arguments[0].Type != TokenType.MemoryLocation & Arguments[0].Type != TokenType.Register)
                        throw new Exception(
                            "Invalid argument 1 for opcode MOV. Expected either a memory address or a register, got " +
                            Arguments[0].Type);
                    if (Arguments[1].Type != TokenType.MemoryLocation & Arguments[1].Type != TokenType.Register &
                        Arguments[1].Type != TokenType.Number)
                        throw new Exception(
                            "Invalid argument 2 for opcode MOV. Expected either a memory address a constant or a register, got " +
                            Arguments[0].Type);

                    return EncodeArgumentLocationEncoding(Arguments, (ushort)InstructionType.Mov);
                case InstructionType.Add:
                    if (Arguments.Count != 2)
                        throw new Exception("Invalid amount of arguments for opcode ADD. 2 expected, got " +
                                            Arguments.Count);
                    if (Arguments[0].Type != TokenType.MemoryLocation & Arguments[0].Type != TokenType.Register)
                        throw new Exception(
                            "Invalid argument 1 for opcode ADD. Expected either a memory address or a register, got " +
                            Arguments[0].Type);
                    if (Arguments[1].Type != TokenType.MemoryLocation & Arguments[1].Type != TokenType.Register &
                        Arguments[1].Type != TokenType.Number)
                        throw new Exception(
                            "Invalid argument 2 for opcode ADD. Expected either a memory address a constant or a register, got " +
                            Arguments[0].Type);

                    return EncodeArgumentLocationEncoding(Arguments, (ushort)InstructionType.Add);
                case InstructionType.Sub:
                    if (Arguments.Count != 2)
                        throw new Exception("Invalid amount of arguments for opcode SUB. 2 expected, got " +
                                            Arguments.Count);
                    if (Arguments[0].Type != TokenType.MemoryLocation & Arguments[0].Type != TokenType.Register)
                        throw new Exception(
                            "Invalid argument 1 for opcode SUB. Expected either a memory address or a register, got " +
                            Arguments[0].Type);
                    if (Arguments[1].Type != TokenType.MemoryLocation & Arguments[1].Type != TokenType.Register &
                        Arguments[1].Type != TokenType.Number)
                        throw new Exception(
                            "Invalid argument 2 for opcode SUB. Expected either a memory address a constant or a register, got " +
                            Arguments[0].Type);

                    return EncodeArgumentLocationEncoding(Arguments, (ushort)InstructionType.Sub);
                case InstructionType.Jmp:
                    if (Arguments.Count != 1)
                        throw new Exception("Invalid amount of arguments for opcode JMP. 1 expected, got " +
                                            Arguments.Count);

                    if (Arguments[0].Type != TokenType.Label & Arguments[0].Type != TokenType.MemoryLocation)
                        throw new Exception(
                            "Invalid argument 1 for opcode JMP. Expected either a memory address or a label, got " +
                            Arguments[0].Type);

                    return new[] { (ushort)InstructionType.Jmp, ushort.Parse(Arguments[0].Data.ToString()) };
                case InstructionType.Cmp:
                    if (Arguments.Count != 2)
                        throw new Exception("Invalid amount of arguments for opcode CMP. 2 expected, got " +
                                            Arguments.Count);
                    if (Arguments[0].Type != TokenType.MemoryLocation & Arguments[0].Type != TokenType.Register)
                        throw new Exception(
                            "Invalid argument 1 for opcode CMP. Expected either a memory address or a register, got " +
                            Arguments[0].Type);
                    if (Arguments[1].Type != TokenType.MemoryLocation & Arguments[1].Type != TokenType.Register &
                        Arguments[1].Type != TokenType.Number)
                        throw new Exception(
                            "Invalid argument 2 for opcode CMP. Expected either a memory address a constant or a register, got " +
                            Arguments[0].Type);

                    return EncodeArgumentLocationEncoding(Arguments, (ushort)InstructionType.Cmp);
                case InstructionType.Je:
                    if (Arguments.Count != 1)
                        throw new Exception("Invalid amount of arguments for opcode JE. 1 expected, got " +
                                            Arguments.Count);
                    if (Arguments[0].Type != TokenType.Label & Arguments[0].Type != TokenType.MemoryLocation)
                        throw new Exception(
                            "Invalid argument 1 for opcode JE. Expected either a memory address or a label, got " +
                            Arguments[0].Type);

                    return new[] { (ushort)InstructionType.Je, ushort.Parse(Arguments[0].Data.ToString()) };
                case InstructionType.Drf:
                    if (Arguments.Count != 2)
                        throw new Exception("Invalid amount of arguments for opcode DRF. 2 expected, got " +
                                            Arguments.Count);
                    if (Arguments[0].Type != TokenType.MemoryLocation & Arguments[0].Type != TokenType.Register)
                        throw new Exception(
                            "Invalid argument 1 for opcode DRF. Expected either a memory address or a register, got " +
                            Arguments[0].Type);
                    if (Arguments[1].Type != TokenType.MemoryLocation & Arguments[1].Type != TokenType.Register)
                        throw new Exception(
                            "Invalid argument 2 for opcode DRF. Expected either a memory address or a register, got " +
                            Arguments[0].Type);

                    return EncodeArgumentLocationEncoding(Arguments, (ushort)InstructionType.Drf);
                case InstructionType.Ptr:
                    if (Arguments.Count != 2)
                        throw new Exception("Invalid amount of arguments for opcode PTR. 2 expected, got " +
                                            Arguments.Count);
                    if (Arguments[0].Type != TokenType.MemoryLocation & Arguments[0].Type != TokenType.Register)
                        throw new Exception(
                            "Invalid argument 1 for opcode PTR. Expected either a memory address or a register, got " +
                            Arguments[0].Type);
                    if (Arguments[1].Type != TokenType.MemoryLocation & Arguments[1].Type != TokenType.Register &
                        Arguments[1].Type != TokenType.Number)
                        throw new Exception(
                            "Invalid argument 2 for opcode PTR. Expected either a memory address a constant or a register, got " +
                            Arguments[0].Type);

                    return EncodeArgumentLocationEncoding(Arguments, (ushort)InstructionType.Ptr);
            }

            return null;
        }
    }
}