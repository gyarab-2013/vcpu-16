﻿namespace VCPU_16
{
    /// <summary>
    /// Defines all the possible token types
    /// </summary>
    public enum TokenType
    {
        Instruction,
        Register,
        Number,
        Label,
        Separator,
        Symbol,
        MemoryLocation
    }

    /// <summary>
    /// This is a class only for the compiler to temporarily store data
    /// </summary>
    internal class Token
    {
        public object Data;
        public TokenType Type;
    }
}