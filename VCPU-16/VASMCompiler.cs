﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace VCPU_16
{
    public static class VASMCompiler
    {
        private static int _charNumInternal;
        private static string _code;
        private static CompilerState _state = CompilerState.WFInstruction;
        private static readonly string[] InstructionList = {"nop", "brk", "hlt", "err", "mov", "add", "sub", "jmp", "cmp", "je", "drf", "ptr"};
        private static readonly Dictionary<string, ushort> Labels = new Dictionary<string, ushort>();

        /// <summary>
        /// Returns the number of the currently processed line
        /// </summary>
        private static int LineNum
        {
            get
            {
                var splits = _code.Split('\n');
                var localPos = _charNumInternal;

                for (var i = 0; i < splits.Length; i++)
                {
                    localPos -= splits[i].Length;
                    if (i != 0) localPos--;
                    if (localPos <= 0) return i + 1;
                }

                return -1;
            }
        }

        /// <summary>
        /// Returns the number of the currently processed character
        /// Results from this field may not be accurate as the compiler regex processes the input in chunks
        /// </summary>
        private static int CharNum
        {
            get
            {
                var splits = _code.Split('\n');
                var localPos = _charNumInternal;

                for (var i = 0; i < splits.Length; i++)
                {
                    localPos -= splits[i].Length;
                    if (i != 0) localPos--;
                    if (localPos <= 0) return localPos + splits[i].Length;
                }

                return -1;
            }
        }

        /*
        Memory location specification: 
        INSTRUCTION:
            1st word:
                1st byte:
                    00000100
                2nd byte:
                    REGN0000: REG, MEM(INSTRUCTION r1, 0001)
                    00000001: REG, REG(INSTRUCTION r1, r2)
                    REGN0010: REG, CONST(INSTRUCTION r1, #10)
                    00000011: MEM, MEM (INSTRUCTION 0001, 0002)
                    REGN0100: MEM, REG(INSTRUCTION 0001, r0)
                    00000101: MEM, CONST(INSTRUCTION 0001, #10)
            2nd word:
                REG, REG: REGNREGN 00000000
                REG, MEM: MEMORYYY ADDRESSS
                REG, CON: COOONSSS TAAAANNT
                MEM, MEM: MEMORYYY ADDRESSS
                MEM, REG: MEMORYYY ADDRESSS
                MEM, CONST: MEMORYYY ADDRESSS
            3rd word:
                MEM, MEM: MEMORYYY ADDRESSS
                MEM, CONST: COOONSSS TAAAANNT
        */

        /// <summary>
        /// This is the main method used for compilation of code
        /// </summary>
        /// <param name="code">Code chunk to be compiled</param>
        /// <returns>Compiled code</returns>
        public static ushort[] Compile(string code)
        {
            LoadString(code);

            _state = CompilerState.WFInstruction;
            Labels.Clear();
            _charNumInternal = 0;

#if DEBUG
            return Parse();
#else
            try
            {
                return Parse();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
#endif
        }

        /// <summary>
        /// Puts the code into a different variable for convinience
        /// </summary>
        /// <param name="code"></param>
        private static void LoadString(string code)
        {
            _code = code;
        }
		
		/// <summary>
        /// Removes things such as comments that are useless for compilation.
        /// </summary>
        /// <param name="code">Code to be processed</param>
        /// <returns>Clean code</returns>
		private static string PreParse(string code)
		{
		    Match match = Regex.Match(code, @"(\/\*(.|\n)*?\*\/)|(\/\/.*)");

		    while (match.Success)
		    {
		        code = code.Replace(match.Value, "");
                match = match.NextMatch();
            }

            return code;
		}

        /// <summary>
        /// Returns a enumerator that returns single group from the input code
        /// This is usually a equivalent of a token
        /// </summary>
        /// <param name="code">Code to be processed</param>
        /// <returns>Enumerator that returns groups</returns>
        private static IEnumerable<string> Scan(string code)
        {
            code = PreParse(code);
			
            var match = Regex.Match(code, @"\#?\w*:?(?=(,|[ \t]|;|$|))", RegexOptions.Multiline);

            while (match.Success)
            {
                _charNumInternal = match.Index;

                if (match.Groups[0].Length == 0) yield return match.Groups[1].Value;
                else yield return match.Groups[0].Value;

                match = match.NextMatch();
            }
        }

        /// <summary>
        /// Checks if the given opcode is a valid instruction
        /// </summary>
        /// <param name="opcode">Opcode to be checked</param>
        /// <returns>True if the opcode if a valid instruction</returns>
        private static bool IsInstruction(string opcode)
        {
            return InstructionList.Any(instruction => instruction == opcode.ToLower());
        }

        /// <summary>
        /// Finds the number of opcode as given in the InstructionType enum
        /// </summary>
        /// <param name="opcode">Opcode to be found</param>
        /// <returns>Number of instruction</returns>
        private static int FindInstruction(string opcode)
        {
            for (var i = 0; i < InstructionList.Length; i++)
            {
                if (InstructionList[i] == opcode.ToLower()) return i;
            }

            return -1;
        }

        /// <summary>
        /// Returns a enumerator that returns single tokens
        /// </summary>
        /// <returns>Enumerator that returns single tokens</returns>
        private static IEnumerable<Token> GetToken()
        {
            foreach (var scan in Scan(_code))
            {
                if (scan == null) break;
                if (scan == "") continue;

                if (scan == ";")
                {
                    if (_state == CompilerState.WFSeparatorArgument | _state == CompilerState.WFSeparatorOpcode)
                        _state = CompilerState.WFInstruction;
                    yield return new Token
                    {
                        Type = TokenType.Symbol,
                        Data = scan
                    };
                    continue;
                }

                if (scan == " " & _state != CompilerState.WFSeparatorOpcode)
                {
                    continue;
                }

                if (scan.EndsWith(":"))
                {
                    //_state = CompilerState.WFSeparatorArgument;

                    yield return new Token
                    {
                        Type = TokenType.Label,
                        Data = scan.TrimEnd(':')
                    };
                    continue;
                }

                switch (_state)
                {
                    case CompilerState.WFInstruction:
                        if (IsInstruction(scan))
                        {
                            _state = CompilerState.WFSeparatorOpcode;
                            yield return new Token
                            {
                                Type = TokenType.Instruction,
                                Data = scan
                            };
                        }
                        else
                        {
                            throw new Exception("Invalid opcode " + "'" + scan + "'");
                        }
                        break;
                    case CompilerState.WFArgument:
                        if (scan.StartsWith("#"))
                        {
                            _state = CompilerState.WFSeparatorArgument;
                            yield return new Token
                            {
                                Type = TokenType.MemoryLocation,
                                Data = scan.Substring(1)
                            };
                        }
                        else if (Regex.Match(scan, "R.*").Success)
                        {
                            _state = CompilerState.WFSeparatorArgument;
                            yield return new Token
                            {
                                Type = TokenType.Register,
                                Data = scan.Substring(1)
                            };
                        }
                        else if (scan == "IP")
                        {
                            _state = CompilerState.WFSeparatorArgument;
                            yield return new Token
                            {
                                Type = TokenType.Register,
                                Data = RegisterType.IP
                            };
                        }
                        else if (scan == "CS")
                        {
                            _state = CompilerState.WFSeparatorArgument;
                            yield return new Token
                            {
                                Type = TokenType.Register,
                                Data = RegisterType.CS
                            };
                        }
                        else if (scan == "DS")
                        {
                            _state = CompilerState.WFSeparatorArgument;
                            yield return new Token
                            {
                                Type = TokenType.Register,
                                Data = RegisterType.DS
                            };
                        }
                        else if (scan == "CR")
                        {
                            _state = CompilerState.WFSeparatorArgument;
                            yield return new Token
                            {
                                Type = TokenType.Register,
                                Data = RegisterType.CR
                            };
                        }
                        else if (Regex.Match(scan, @"\d*").Success)
                        {
                            _state = CompilerState.WFSeparatorArgument;
                            yield return new Token
                            {
                                Type = TokenType.Number,
                                Data = scan
                            };
                        }
                        else
                        {
                            throw new Exception("Unexpected argument on line " + LineNum + " character " + CharNum +
                                                ": " + scan);
                        }
                        break;
                    case CompilerState.WFSeparatorArgument:
                        if (scan != ",")
                            throw new Exception("Comma expected on line " + LineNum + " character " + CharNum);
                        _state = CompilerState.WFArgument;
                        yield return new Token
                        {
                            Data = ",",
                            Type = TokenType.Separator
                        };
                        break;
                    case CompilerState.WFSeparatorOpcode:
                        if (scan != " ")
                            throw new Exception("Space expected on line " + LineNum + " character " + CharNum);
                        _state = CompilerState.WFArgument;
                        yield return new Token
                        {
                            Data = " ",
                            Type = TokenType.Separator
                        };
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        /// <summary>
        /// Assembles the abstarct syntax tree
        /// </summary>
        /// <returns>Non-compiled list of instructions in the currently loaded code</returns>
        private static List<Instruction> AssembleTree()
        {
            var tree = new List<Instruction>();
            Instruction currentInstruction = null;


            foreach (var token in GetToken())
            {
                Console.WriteLine(LineNum + ": " + token.Type + ": " + "'" + token.Data + "'");

                switch (token.Type)
                {
                    case TokenType.Register:
                        var regNum = int.Parse((string) token.Data);
                        if (regNum > 15 | regNum < 0)
                            throw new Exception("Invalid regsiter on line " + LineNum + " character " + CharNum +
                                                " - (R" + regNum + " is not in range of R0 - R15)");
                        break;
                    case TokenType.MemoryLocation:
                        var memLoc = int.Parse((string) token.Data);
                        if (memLoc > 0xFFFF | memLoc < 0)
                            throw new Exception("Invalid memory location on line " + LineNum + " character " + CharNum +
                                                " - (#" + memLoc + " is not in range of 0x0 - 0xFFFF)");
                        break;
                }

                if (token.Type == TokenType.Instruction)
                {
                    currentInstruction = new Instruction();
                    currentInstruction.Type = (InstructionType) FindInstruction((string) token.Data);

                    if ((int) currentInstruction.Type == -1)
                        throw new Exception(
                            "Invalid opcode that got through first stage parsing, most likely a compiler error. Please report this to virusek20");
                }

                if (token.Type == TokenType.MemoryLocation | token.Type == TokenType.Number |
                    token.Type == TokenType.Register)
                {
                    if (currentInstruction == null)
                        throw new Exception(
                            "Got a argument that doesn't have a matching instruction that got throught first stage parsing. This is most likely a compile error. Please report this to virusek20");
                    currentInstruction.Arguments.Add(token);
                }

                if (token.Type == TokenType.Symbol & (string) token.Data == ";")
                {
                    Console.WriteLine("Adding instruction " + currentInstruction.Type + " to the tree");
                    tree.Add(currentInstruction);
                    currentInstruction = null;
                }
            }

            return tree;
        }

        /// <summary>
        /// Builds the binary representation of the abstract syntax tree
        /// </summary>
        /// <param name="tree">Abstract syntax tree to be evaluated</param>
        /// <returns>Compiled code</returns>
        private static ushort[] EvaluateTree(IEnumerable<Instruction> tree)
        {
            return tree.SelectMany(instruction => instruction.AssembleByteCode()).ToArray();
        }

        /// <summary>
        /// Starts the compilation process
        /// </summary>
        /// <returns></returns>
        private static ushort[] Parse()
        {
            return EvaluateTree(AssembleTree());
        }

        /// <summary>
        /// Defines all the possible states of the compiler
        /// WF stands for: "Waiting for"
        /// </summary>
        private enum CompilerState
        {
            WFInstruction,
            WFArgument,
            WFSeparatorArgument,
            WFSeparatorOpcode
        }
    }
}