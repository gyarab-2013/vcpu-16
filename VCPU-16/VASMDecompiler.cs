﻿namespace VCPU_16
{
    public static class VASMDecompiler
    {
        private const int OpCodeMask = 255;
        private const int LocationEncodingMask = 3840;
        private const int LocationEncodingShift = 8;
        private const int SingleRegisterMask = 61440;
        private const int SingleRegisterShift = 12;
        private const int FirstRegisterMask = 65280;
        private const int SecondRegisterMask = 255;
        private const int DoubleRegisterFirstArgumentShift = 8;

        /// <summary>
        /// This method returns the register associated with an opcode in case the opcode location encoding is using exactly 1 register.
        /// This method does NOT check for invalid opcodes and different location encodings.
        /// </summary>
        /// <param name="opcode">Target opcode to be disassembled</param>
        /// <returns>Register used as a argument</returns>
        private static ushort GetMemoryRegisterLocation(ushort opcode)
        {
            return (ushort) ((opcode & SingleRegisterMask) >> SingleRegisterShift);
        }

        /// <summary>
        /// Gets the string describing the location encoding of a opcode at a given address.
        /// </summary>
        /// <param name="cpu">The cpu from which you're dissassembling</param>
        /// <param name="address">The address of the opcode to be disassembled</param>
        /// <returns>String describing the location encoding of a opcode</returns>
        private static string GetArgumentLocationEncodingString(VCPU cpu, ushort address, ref ushort next)
        {
            string finalString = "";

            ushort memoryType = (ushort)((cpu.GetMemory(address) & LocationEncodingMask) >> LocationEncodingShift);

            switch ((ArgumentLocationEncodingType)memoryType)
            {
                case ArgumentLocationEncodingType.RegisterAndMemory:
                    finalString += "R" + GetMemoryRegisterLocation(cpu.GetMemory(address));
                    finalString += ", ";
                    finalString += "#" + cpu.GetMemory((ushort)(address + 1));

                    next++;
                    return finalString;
                case ArgumentLocationEncodingType.RegisterAndRegister:
                    finalString += "R" + ((cpu.GetMemory((ushort)((ushort)(address + 1))) & FirstRegisterMask) >> DoubleRegisterFirstArgumentShift);
                    finalString += ", ";
                    finalString += "R" + (cpu.GetMemory((ushort)(address + 1)) & SecondRegisterMask);

                    next++;
                    return finalString;
                case ArgumentLocationEncodingType.RegisterAndConst:
                    finalString += "R" + GetMemoryRegisterLocation(cpu.GetMemory(address));
                    finalString += ", ";
                    finalString += cpu.GetMemory((ushort)(address + 1)).ToString();

                    next++;
                    return finalString;
                case ArgumentLocationEncodingType.MemoryAndMemory:
                    finalString += "#" + cpu.GetMemory((ushort)(address + 1));
                    finalString += ", ";
                    finalString += "# " + cpu.GetMemory((ushort)(address + 2));

                    next += 2;
                    return finalString;
                case ArgumentLocationEncodingType.MemoryAndRegister:
                    finalString += "#" + cpu.GetMemory((ushort)(address + 1));
                    finalString += ", ";
                    finalString += "R" + GetMemoryRegisterLocation(cpu.GetMemory(address));

                    next++;
                    return finalString;
                case ArgumentLocationEncodingType.MemoryAndConstant:
                    finalString += "#" + cpu.GetMemory((ushort)(address + 1));
                    finalString += ", ";
                    finalString += cpu.GetMemory((ushort)(address + 2));

                    next += 2;
                    return finalString;
            }

            return finalString;
        }

        /// <summary>
        /// Decompiles a opcode at a given address.
        /// </summary>
        /// <param name="cpu">The cpu from which you're dissassembling</param>
        /// <param name="address">The address of the opcode to be disassembled</param>
        /// <returns>Disassembled instruction</returns>
        public static string DecompileOpcode(VCPU cpu, ushort address, out ushort next)
        {
            next = 0;

            switch ((InstructionType)(cpu.GetMemory(address) & OpCodeMask))
            {
                case InstructionType.Nop:
                    return "NOP";
                case InstructionType.Brk:
                    return "BRK";
                case InstructionType.Hlt:
                    return "HLT";
                case InstructionType.Err:
                    return "ERR";
                case InstructionType.Mov:
                    return "MOV " + GetArgumentLocationEncodingString(cpu, address, ref next);
                case InstructionType.Add:
                    return "ADD " + GetArgumentLocationEncodingString(cpu, address, ref next);
                case InstructionType.Sub:
                    return "SUB " + GetArgumentLocationEncodingString(cpu, address, ref next);
                case InstructionType.Jmp:
                    next = 1;
                    return "JMP #" + cpu.GetMemory((ushort)(address + 1));
                case InstructionType.Cmp:
                    return "CMP " + GetArgumentLocationEncodingString(cpu, address, ref next);
                case InstructionType.Je:
                    next = 1;
                    return "JE #" +cpu.GetMemory((ushort)(address + 1));
                case InstructionType.Drf:
                    return "DRF " + GetArgumentLocationEncodingString(cpu, address, ref next);
            }

            return "Invalid opcode";
        }
    }
}