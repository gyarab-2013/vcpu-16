﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace VCPU_16
{
    /// <summary>
    /// Defines all the different registers and their number in the Register variable
    /// </summary>
    public enum RegisterType
    {
        R0,
        R1,
        R2,
        R3,
        R4,
        R5,
        R6,
        R7,
        R8,
        R9,
        R10,
        R11,
        R12,
        R13,
        R14,
        R15,
        IP,
        CS,
        DS,
        CR
    }

    public class VCPU
    {
        private const int OpCodeMask = 255;
        private const int LocationEncodingMask = 3840;
        private const int LocationEncodingShift = 8;
        private const int SingleRegisterMask = 61440;
        private const int SingleRegisterShift = 12;
        private const int FirstRegisterMask = 65280;
        private const int SecondRegisterMask = 255;
        private const int DoubleRegisterFirstArgumentShift = 8;

        public delegate void MemorySynchronizationDelegate();

        public bool Running;

        public ushort MemorySize;
        public bool MemorySync = true;
        private readonly ushort[] _memory;

        public MemorySynchronizationDelegate OnMemorySynchronization;

        /// <summary>
        /// All registers as defined in the RegisterType enum
        /// </summary>
        public ushort[] Registers = new ushort[20];

        public Stack<ushort> Stack = new Stack<ushort>();

        /// <summary>
        /// Initializes a new CPU and sets it's memory size
        /// </summary>
        /// <param name="memorySize">Size of the CPU memory in words (16 bits / 2 bytes)</param>
        public VCPU(ushort memorySize)
        {
            MemorySize = memorySize;

            _memory = new ushort[memorySize];
        }

        /// <summary>
        /// Returns a single word from the memory
        /// In order to use this method the CPU must be halted and the MemorySync field has to be set to true to prevent memory problems
        /// </summary>
        /// <param name="address">Adress from which to load data</param>
        /// <returns>Data at given address</returns>
        public ushort GetMemory(ushort address)
        {
            if (!MemorySync)
                throw new AccessViolationException(
                    "You cannot access this CPU's memory unless the MemorySync field is set to true.");
            if (address > _memory.Length)
                throw new IndexOutOfRangeException("You cannot access a memory position beyond the memory of the CPU.");

            return _memory[address];
        }

        /// <summary>
        /// Sets a single word in the memory
        /// In order to use this method the CPU must be halted and the MemorySync field has to be set to true to prevent memory problems
        /// </summary>
        /// <param name="address">Address to which to save data</param>
        /// <param name="value">Data to sace</param>
        /// <returns>Previous data located at given address</returns>
        public ushort SetMemory(ushort address, ushort value)
        {
            if (!MemorySync)
                throw new AccessViolationException(
                    "You cannot access this CPU's memory unless the MemorySync field is set to true.");
            if (address > _memory.Length)
                throw new IndexOutOfRangeException("You cannot access a memory position beyond the memory of the CPU.");

            var oldValue = _memory[address];
            _memory[address] = value;

            return oldValue;
        }

        /// <summary>
        /// Performs a single step, processing single instruction
        /// </summary>
        public void Step()
        {
            if (!Running) return;
            if (MemorySync) return;

            InternalStep();
        }

        /// <summary>
        /// Internal way of performing steps
        /// This method is used to reduce the time required to process opcodes
        /// </summary>
        private void InternalStep()
        {
            if (Registers[(int) RegisterType.IP] >= _memory.Length)
            {
                Running = false;
                MemorySync = true;
                if (OnMemorySynchronization != null) OnMemorySynchronization.Invoke();
                return;
            }

            EvaluateOpCode(_memory[Registers[(int)RegisterType.IP]++]);
        }

        /// <summary>
        /// Starts the CPU on a different thread
        /// </summary>
        public void Run()
        {
            MemorySync = false;
            Running = true;

            var runThread = new Thread(() =>
            {
                while (Running)
                {
                    if (!MemorySync) InternalStep();
                }
            });

            runThread.Start();
        }

        /// <summary>
        /// Evaluates single opcode and sets the CPU state accordingly
        /// </summary>
        /// <param name="opCode">Opcode to evaluate</param>
        private void EvaluateOpCode(ushort opCode)
        {
            int registerNumber = (opCode & SingleRegisterMask) >> SingleRegisterShift;

            switch ((InstructionType)(opCode & OpCodeMask))
            {
                case InstructionType.Nop:
                    break;
                case InstructionType.Brk:
                    MemorySync = true;
                    if (OnMemorySynchronization != null) OnMemorySynchronization.Invoke();
                    MemorySync = false;
                    break;
                case InstructionType.Hlt:
                    Running = false;
                    MemorySync = true;
                    if (OnMemorySynchronization != null) OnMemorySynchronization.Invoke();
                    break;
                case InstructionType.Err:
                    throw new Exception("CPU processed the ERR opcode! I shall now halt and catch fire.");
                case InstructionType.Mov:
                    #region MOV
                    switch ((ArgumentLocationEncodingType) ((opCode & LocationEncodingMask) >> LocationEncodingShift))
                    {
                        case ArgumentLocationEncodingType.RegisterAndMemory:
                            if (registerNumber == 16) throw new Exception("Cannot directly modify the IP register, use the JMP instruction instead");

                            Registers[registerNumber] = _memory[_memory[Registers[(int)RegisterType.IP]]];
                            Registers[(int)RegisterType.IP]++;
                            break;
                        case ArgumentLocationEncodingType.MemoryAndConstant:
                            _memory[_memory[Registers[(int)RegisterType.IP]]] = _memory[Registers[(int)RegisterType.IP] + 1];
                            Registers[(int)RegisterType.IP] += 2;
                            break;
                        case ArgumentLocationEncodingType.MemoryAndMemory:
                            _memory[_memory[Registers[(int)RegisterType.IP]]] = _memory[_memory[Registers[(int)RegisterType.IP] + 1]];
                            Registers[(int)RegisterType.IP] += 2;
                            break;
                        case ArgumentLocationEncodingType.RegisterAndConst:
                            if (registerNumber == 16) throw new Exception("Cannot directly modify the IP register, use the JMP instruction instead");

                            Registers[registerNumber] = _memory[Registers[(int)RegisterType.IP]];
                            Registers[(int)RegisterType.IP]++;
                            break;
                        case ArgumentLocationEncodingType.MemoryAndRegister:
                            _memory[_memory[Registers[(int)RegisterType.IP]]] = Registers[registerNumber];
                            Registers[(int)RegisterType.IP]++;
                            break;
                        case ArgumentLocationEncodingType.RegisterAndRegister:
                            int firstRegister = (_memory[Registers[(int)RegisterType.IP]] & FirstRegisterMask) >> DoubleRegisterFirstArgumentShift;
                            int secondRegister = _memory[Registers[(int)RegisterType.IP]] & SecondRegisterMask;

                            if (firstRegister == 16 | secondRegister == 16) throw new Exception("Cannot directly modify the IP register, use the JMP instruction instead");

                            Registers[firstRegister] = Registers[secondRegister];
                            Registers[(int)RegisterType.IP]++;
                            break;
                    }
                    #endregion
                    break;
                case InstructionType.Add:
                    #region ADD
                    switch ((ArgumentLocationEncodingType)((opCode & LocationEncodingMask) >> LocationEncodingShift))
                    {
                        case ArgumentLocationEncodingType.RegisterAndMemory:
                            if (registerNumber == 16) throw new Exception("Cannot directly modify the IP register, use the JMP instruction instead");
                            Registers[registerNumber] += _memory[_memory[Registers[(int)RegisterType.IP]]];
                            Registers[(int)RegisterType.IP]++;
                            break;
                        case ArgumentLocationEncodingType.MemoryAndConstant:
                            _memory[_memory[Registers[(int)RegisterType.IP]]] += _memory[Registers[(int)RegisterType.IP] + 1];
                            Registers[(int)RegisterType.IP] += 2;
                            break;
                        case ArgumentLocationEncodingType.MemoryAndMemory:
                            _memory[_memory[Registers[(int)RegisterType.IP]]] += _memory[_memory[Registers[(int)RegisterType.IP] + 1]];
                            Registers[(int)RegisterType.IP] += 2;
                            break;
                        case ArgumentLocationEncodingType.RegisterAndConst:
                            if (registerNumber == 16) throw new Exception("Cannot directly modify the IP register, use the JMP instruction instead");
                            Registers[registerNumber] += _memory[Registers[(int)RegisterType.IP]];
                            Registers[(int)RegisterType.IP]++;
                            break;
                        case ArgumentLocationEncodingType.MemoryAndRegister:
                            _memory[_memory[Registers[(int)RegisterType.IP]]] += Registers[registerNumber];
                            Registers[(int)RegisterType.IP]++;
                            break;
                        case ArgumentLocationEncodingType.RegisterAndRegister:
                            int firstRegister = (_memory[Registers[(int)RegisterType.IP]] & FirstRegisterMask) >> DoubleRegisterFirstArgumentShift;
                            int secondRegister = _memory[Registers[(int)RegisterType.IP]] & SecondRegisterMask;

                            if (firstRegister == 16 | secondRegister == 16) throw new Exception("Cannot directly modify the IP register, use the JMP instruction instead");

                            Registers[firstRegister] += Registers[secondRegister];
                            Registers[(int)RegisterType.IP]++;
                            break;
                    }
                    #endregion
                    break;
                case InstructionType.Sub:
                    #region SUB
                    switch ((ArgumentLocationEncodingType)((opCode & LocationEncodingMask) >> LocationEncodingShift))
                    {
                        case ArgumentLocationEncodingType.RegisterAndMemory:
                            if (registerNumber == 16) throw new Exception("Cannot directly modify the IP register, use the JMP instruction instead");
                            Registers[registerNumber] -= _memory[_memory[Registers[(int)RegisterType.IP]]];
                            Registers[(int)RegisterType.IP]++;
                            break;
                        case ArgumentLocationEncodingType.MemoryAndConstant:
                            _memory[_memory[Registers[(int)RegisterType.IP]]] -= _memory[Registers[(int)RegisterType.IP] + 1];
                            Registers[(int)RegisterType.IP] += 2;
                            break;
                        case ArgumentLocationEncodingType.MemoryAndMemory:
                            _memory[_memory[Registers[(int)RegisterType.IP]]] -= _memory[_memory[Registers[(int)RegisterType.IP] + 1]];
                            Registers[(int)RegisterType.IP] += 2;
                            break;
                        case ArgumentLocationEncodingType.RegisterAndConst:
                            if (registerNumber == 16) throw new Exception("Cannot directly modify the IP register, use the JMP instruction instead");
                            Registers[registerNumber] -= _memory[Registers[(int)RegisterType.IP]];
                            Registers[(int)RegisterType.IP]++;
                            break;
                        case ArgumentLocationEncodingType.MemoryAndRegister:
                            _memory[_memory[Registers[(int)RegisterType.IP]]] -= Registers[registerNumber];
                            Registers[(int)RegisterType.IP]++;
                            break;
                        case ArgumentLocationEncodingType.RegisterAndRegister:
                            int firstRegister = (_memory[Registers[(int)RegisterType.IP]] & FirstRegisterMask) >> DoubleRegisterFirstArgumentShift;
                            int secondRegister = _memory[Registers[(int)RegisterType.IP]] & SecondRegisterMask;

                            if (firstRegister == 16 | secondRegister == 16) throw new Exception("Cannot directly modify the IP register, use the JMP instruction instead");

                            Registers[firstRegister] -= Registers[secondRegister];
                            Registers[(int)RegisterType.IP]++;
                            break;
                    }
                    #endregion
                    break;
                case InstructionType.Jmp:
                    Registers[(int)RegisterType.IP] = _memory[Registers[(int)RegisterType.IP]];
                    break;
                case InstructionType.Cmp:
                    #region CMP
                    switch ((ArgumentLocationEncodingType)((opCode & LocationEncodingMask) >> LocationEncodingShift))
                    {
                        case ArgumentLocationEncodingType.RegisterAndMemory:
                            Registers[(int)RegisterType.CR] = Registers[registerNumber] == _memory[_memory[Registers[(int)RegisterType.IP]]] ? (ushort) 1 : (ushort) 0;
                            break;
                        case ArgumentLocationEncodingType.MemoryAndConstant:
                            Registers[(int)RegisterType.CR] = _memory[_memory[Registers[(int)RegisterType.IP]]] == _memory[Registers[(int)RegisterType.IP] + 1] ? (ushort) 1 : (ushort) 0;
                            Registers[(int)RegisterType.IP] += 2;
                            break;
                        case ArgumentLocationEncodingType.MemoryAndMemory:
                            Registers[(int)RegisterType.CR] = _memory[_memory[Registers[(int)RegisterType.IP]]] == _memory[_memory[Registers[(int)RegisterType.IP] + 1]] ? (ushort) 1 : (ushort) 0;
                            Registers[(int)RegisterType.IP] += 2;
                            break;
                        case ArgumentLocationEncodingType.RegisterAndConst:
                            Registers[(int)RegisterType.CR] = Registers[registerNumber] == _memory[Registers[(int)RegisterType.IP]] ? (ushort) 1 : (ushort) 0;
                            Registers[(int)RegisterType.IP]++;
                            break;
                        case ArgumentLocationEncodingType.MemoryAndRegister:
                            Registers[(int)RegisterType.CR] = _memory[_memory[Registers[(int)RegisterType.IP]]] == Registers[registerNumber] ? (ushort) 1 : (ushort) 0;
                            Registers[(int)RegisterType.IP]++;
                            break;
                        case ArgumentLocationEncodingType.RegisterAndRegister:
                            int firstRegister = (_memory[Registers[(int)RegisterType.IP]] & FirstRegisterMask) >> DoubleRegisterFirstArgumentShift;
                            int secondRegister = _memory[Registers[(int)RegisterType.IP]] & SecondRegisterMask;

                            Registers[(int)RegisterType.CR] = Registers[firstRegister] == Registers[secondRegister] ? (ushort) 1 : (ushort) 0;
                            Registers[(int)RegisterType.IP]++;
                            break;
                    }
                    #endregion
                    break;
                case InstructionType.Je:
                    if (Registers[(int)RegisterType.CR] == 1) Registers[(int)RegisterType.IP] = _memory[Registers[(int)RegisterType.IP]];
                    break;
                case InstructionType.Drf:
                    #region DRF
                    switch ((ArgumentLocationEncodingType)((opCode & LocationEncodingMask) >> LocationEncodingShift))
                    {
                        case ArgumentLocationEncodingType.RegisterAndMemory:
                            if (registerNumber == 16) throw new Exception("Cannot directly modify the IP register, use the JMP instruction instead");

                            Registers[registerNumber] = _memory[_memory[_memory[Registers[(int)RegisterType.IP]]]];
                            Registers[(int)RegisterType.IP]++;
                            break;
                        case ArgumentLocationEncodingType.MemoryAndMemory:
                            _memory[_memory[Registers[(int)RegisterType.IP]]] = _memory[_memory[_memory[Registers[(int)RegisterType.IP] + 1]]];
                            Registers[(int)RegisterType.IP] += 2;
                            break;
                        case ArgumentLocationEncodingType.MemoryAndRegister:
                            _memory[_memory[Registers[(int)RegisterType.IP]]] = _memory[Registers[registerNumber]];
                            Registers[(int)RegisterType.IP]++;
                            break;
                        case ArgumentLocationEncodingType.RegisterAndRegister:
                            int firstRegister = (_memory[Registers[(int)RegisterType.IP]] & FirstRegisterMask) >> DoubleRegisterFirstArgumentShift;
                            int secondRegister = _memory[Registers[(int)RegisterType.IP]] & SecondRegisterMask;

                            if (firstRegister == 16 | secondRegister == 16) throw new Exception("Cannot directly modify the IP register, use the JMP instruction instead");

                            Registers[firstRegister] = _memory[Registers[secondRegister]];
                            Registers[(int)RegisterType.IP]++;
                            break;
                    }
                    #endregion
                    break;
                case InstructionType.Ptr:
                    #region PTR
                    switch ((ArgumentLocationEncodingType)((opCode & LocationEncodingMask) >> LocationEncodingShift))
                    {
                        case ArgumentLocationEncodingType.RegisterAndMemory:
                            if (registerNumber == 16) throw new Exception("Cannot directly modify the IP register, use the JMP instruction instead");

                            Registers[registerNumber] = _memory[_memory[Registers[(int)RegisterType.IP]]];
                            Registers[(int)RegisterType.IP]++;
                            break;
                        case ArgumentLocationEncodingType.MemoryAndMemory:
                            _memory[_memory[Registers[(int)RegisterType.IP]]] = _memory[_memory[Registers[(int)RegisterType.IP] + 1]];
                            Registers[(int)RegisterType.IP] += 2;
                            break;
                        case ArgumentLocationEncodingType.MemoryAndRegister:
                            _memory[_memory[Registers[(int)RegisterType.IP]]] = Registers[registerNumber];
                            Registers[(int)RegisterType.IP]++;
                            break;
                        case ArgumentLocationEncodingType.RegisterAndRegister:
                            int firstRegister = (_memory[Registers[(int)RegisterType.IP]] & FirstRegisterMask) >> DoubleRegisterFirstArgumentShift;
                            int secondRegister = _memory[Registers[(int)RegisterType.IP]] & SecondRegisterMask;

                            if (firstRegister == 16 | secondRegister == 16) throw new Exception("Cannot directly modify the IP register, use the JMP instruction instead");

                            Registers[firstRegister] = Registers[secondRegister];
                            Registers[(int)RegisterType.IP]++;
                            break;
                    }
                    #endregion
                    break;
            }
        }
    }
}