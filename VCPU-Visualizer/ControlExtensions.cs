﻿using System.Reflection;
using System.Windows.Forms;

namespace VCPU_Visualizer
{
    public static class ControlExtensions
    {
        public static void DoubleBuffer(this Control control)
        {
            if (SystemInformation.TerminalServerSession) return;
            var dbProp = typeof (Control).GetProperty("DoubleBuffered", BindingFlags.NonPublic | BindingFlags.Instance);
            dbProp.SetValue(control, true, null);
        }
    }
}