﻿namespace VCPU_Visualizer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.Address = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Hexadecimal = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Decimal = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listView2 = new System.Windows.Forms.ListView();
            this.Address_Code_Hex = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Code = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newCPUToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveCPUStateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadCPUStateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.CRTextBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.DSTextBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.CSTextBox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.IPTextBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.R15TextBox = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.R14TextBox = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.R13TextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.R12TextBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.R11TextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.R10TextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.R9TextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.R8TextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.R7TextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.R6TextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.R5TextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.R4TextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.R3TextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.R2TextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.R1TextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.R0TextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Address_Code_Dec = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Address,
            this.Hexadecimal,
            this.Decimal});
            this.listView1.Dock = System.Windows.Forms.DockStyle.Right;
            this.listView1.Location = new System.Drawing.Point(566, 24);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(218, 358);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // Address
            // 
            this.Address.Text = "Address";
            // 
            // Hexadecimal
            // 
            this.Hexadecimal.Text = "Hexadecimal";
            this.Hexadecimal.Width = 81;
            // 
            // Decimal
            // 
            this.Decimal.Text = "Decimal";
            // 
            // listView2
            // 
            this.listView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Address_Code_Hex,
            this.Address_Code_Dec,
            this.Code});
            this.listView2.Dock = System.Windows.Forms.DockStyle.Left;
            this.listView2.Location = new System.Drawing.Point(0, 24);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(203, 358);
            this.listView2.TabIndex = 1;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            // 
            // Address_Code_Hex
            // 
            this.Address_Code_Hex.Text = "Address (HEX)";
            this.Address_Code_Hex.Width = 63;
            // 
            // Code
            // 
            this.Code.Text = "Code";
            this.Code.Width = 85;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newCPUToolStripMenuItem,
            this.saveCPUStateToolStripMenuItem,
            this.loadCPUStateToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newCPUToolStripMenuItem
            // 
            this.newCPUToolStripMenuItem.Name = "newCPUToolStripMenuItem";
            this.newCPUToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newCPUToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.newCPUToolStripMenuItem.Text = "New CPU";
            // 
            // saveCPUStateToolStripMenuItem
            // 
            this.saveCPUStateToolStripMenuItem.Name = "saveCPUStateToolStripMenuItem";
            this.saveCPUStateToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveCPUStateToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.saveCPUStateToolStripMenuItem.Text = "Save CPU State";
            // 
            // loadCPUStateToolStripMenuItem
            // 
            this.loadCPUStateToolStripMenuItem.Name = "loadCPUStateToolStripMenuItem";
            this.loadCPUStateToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.loadCPUStateToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.loadCPUStateToolStripMenuItem.Text = "Load CPU State";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(195, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBox1.DetectUrls = false;
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.richTextBox1.ForeColor = System.Drawing.Color.Lime;
            this.richTextBox1.Location = new System.Drawing.Point(203, 24);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.richTextBox1.Size = new System.Drawing.Size(363, 358);
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.Text = "Ahoj světe!";
            this.richTextBox1.WordWrap = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox15);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.CRTextBox);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.DSTextBox);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.CSTextBox);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.IPTextBox);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.R15TextBox);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.R14TextBox);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.R13TextBox);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.R12TextBox);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.R11TextBox);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.R10TextBox);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.R9TextBox);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.R8TextBox);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.R7TextBox);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.R6TextBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.R5TextBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.R4TextBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.R3TextBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.R2TextBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.R1TextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.R0TextBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(203, 244);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(363, 138);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Registers";
            // 
            // textBox15
            // 
            this.textBox15.Enabled = false;
            this.textBox15.Location = new System.Drawing.Point(300, 111);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(43, 20);
            this.textBox15.TabIndex = 41;
            this.textBox15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(310, 95);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(22, 13);
            this.label15.TabIndex = 40;
            this.label15.Text = "-----";
            // 
            // CRTextBox
            // 
            this.CRTextBox.Enabled = false;
            this.CRTextBox.Location = new System.Drawing.Point(251, 111);
            this.CRTextBox.Name = "CRTextBox";
            this.CRTextBox.Size = new System.Drawing.Size(43, 20);
            this.CRTextBox.TabIndex = 39;
            this.CRTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(261, 95);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(22, 13);
            this.label16.TabIndex = 38;
            this.label16.Text = "CR";
            // 
            // DSTextBox
            // 
            this.DSTextBox.Enabled = false;
            this.DSTextBox.Location = new System.Drawing.Point(202, 111);
            this.DSTextBox.Name = "DSTextBox";
            this.DSTextBox.Size = new System.Drawing.Size(43, 20);
            this.DSTextBox.TabIndex = 37;
            this.DSTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(212, 95);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(22, 13);
            this.label17.TabIndex = 36;
            this.label17.Text = "DS";
            // 
            // CSTextBox
            // 
            this.CSTextBox.Enabled = false;
            this.CSTextBox.Location = new System.Drawing.Point(153, 111);
            this.CSTextBox.Name = "CSTextBox";
            this.CSTextBox.Size = new System.Drawing.Size(43, 20);
            this.CSTextBox.TabIndex = 35;
            this.CSTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(163, 95);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(21, 13);
            this.label18.TabIndex = 34;
            this.label18.Text = "CS";
            // 
            // IPTextBox
            // 
            this.IPTextBox.Enabled = false;
            this.IPTextBox.Location = new System.Drawing.Point(104, 111);
            this.IPTextBox.Name = "IPTextBox";
            this.IPTextBox.Size = new System.Drawing.Size(43, 20);
            this.IPTextBox.TabIndex = 33;
            this.IPTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(114, 95);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(17, 13);
            this.label19.TabIndex = 32;
            this.label19.Text = "IP";
            // 
            // R15TextBox
            // 
            this.R15TextBox.Enabled = false;
            this.R15TextBox.Location = new System.Drawing.Point(55, 111);
            this.R15TextBox.Name = "R15TextBox";
            this.R15TextBox.Size = new System.Drawing.Size(43, 20);
            this.R15TextBox.TabIndex = 31;
            this.R15TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(65, 95);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(27, 13);
            this.label20.TabIndex = 30;
            this.label20.Text = "R15";
            // 
            // R14TextBox
            // 
            this.R14TextBox.Enabled = false;
            this.R14TextBox.Location = new System.Drawing.Point(6, 111);
            this.R14TextBox.Name = "R14TextBox";
            this.R14TextBox.Size = new System.Drawing.Size(43, 20);
            this.R14TextBox.TabIndex = 29;
            this.R14TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(16, 95);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(27, 13);
            this.label21.TabIndex = 28;
            this.label21.Text = "R14";
            // 
            // R13TextBox
            // 
            this.R13TextBox.Enabled = false;
            this.R13TextBox.Location = new System.Drawing.Point(300, 74);
            this.R13TextBox.Name = "R13TextBox";
            this.R13TextBox.Size = new System.Drawing.Size(43, 20);
            this.R13TextBox.TabIndex = 27;
            this.R13TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(310, 58);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(27, 13);
            this.label14.TabIndex = 26;
            this.label14.Text = "R13";
            // 
            // R12TextBox
            // 
            this.R12TextBox.Enabled = false;
            this.R12TextBox.Location = new System.Drawing.Point(251, 74);
            this.R12TextBox.Name = "R12TextBox";
            this.R12TextBox.Size = new System.Drawing.Size(43, 20);
            this.R12TextBox.TabIndex = 25;
            this.R12TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(261, 58);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "R12";
            // 
            // R11TextBox
            // 
            this.R11TextBox.Enabled = false;
            this.R11TextBox.Location = new System.Drawing.Point(202, 74);
            this.R11TextBox.Name = "R11TextBox";
            this.R11TextBox.Size = new System.Drawing.Size(43, 20);
            this.R11TextBox.TabIndex = 23;
            this.R11TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(212, 58);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "R11";
            // 
            // R10TextBox
            // 
            this.R10TextBox.Enabled = false;
            this.R10TextBox.Location = new System.Drawing.Point(153, 74);
            this.R10TextBox.Name = "R10TextBox";
            this.R10TextBox.Size = new System.Drawing.Size(43, 20);
            this.R10TextBox.TabIndex = 21;
            this.R10TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(163, 58);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "R10";
            // 
            // R9TextBox
            // 
            this.R9TextBox.Enabled = false;
            this.R9TextBox.Location = new System.Drawing.Point(104, 74);
            this.R9TextBox.Name = "R9TextBox";
            this.R9TextBox.Size = new System.Drawing.Size(43, 20);
            this.R9TextBox.TabIndex = 19;
            this.R9TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(114, 58);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(21, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "R9";
            // 
            // R8TextBox
            // 
            this.R8TextBox.Enabled = false;
            this.R8TextBox.Location = new System.Drawing.Point(55, 74);
            this.R8TextBox.Name = "R8TextBox";
            this.R8TextBox.Size = new System.Drawing.Size(43, 20);
            this.R8TextBox.TabIndex = 17;
            this.R8TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(65, 58);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "R8";
            // 
            // R7TextBox
            // 
            this.R7TextBox.Enabled = false;
            this.R7TextBox.Location = new System.Drawing.Point(6, 74);
            this.R7TextBox.Name = "R7TextBox";
            this.R7TextBox.Size = new System.Drawing.Size(43, 20);
            this.R7TextBox.TabIndex = 15;
            this.R7TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 58);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "R7";
            // 
            // R6TextBox
            // 
            this.R6TextBox.Enabled = false;
            this.R6TextBox.Location = new System.Drawing.Point(300, 36);
            this.R6TextBox.Name = "R6TextBox";
            this.R6TextBox.Size = new System.Drawing.Size(43, 20);
            this.R6TextBox.TabIndex = 13;
            this.R6TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(310, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "R6";
            // 
            // R5TextBox
            // 
            this.R5TextBox.Enabled = false;
            this.R5TextBox.Location = new System.Drawing.Point(251, 36);
            this.R5TextBox.Name = "R5TextBox";
            this.R5TextBox.Size = new System.Drawing.Size(43, 20);
            this.R5TextBox.TabIndex = 11;
            this.R5TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(261, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "R5";
            // 
            // R4TextBox
            // 
            this.R4TextBox.Enabled = false;
            this.R4TextBox.Location = new System.Drawing.Point(202, 36);
            this.R4TextBox.Name = "R4TextBox";
            this.R4TextBox.Size = new System.Drawing.Size(43, 20);
            this.R4TextBox.TabIndex = 9;
            this.R4TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(212, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "R4";
            // 
            // R3TextBox
            // 
            this.R3TextBox.Enabled = false;
            this.R3TextBox.Location = new System.Drawing.Point(153, 36);
            this.R3TextBox.Name = "R3TextBox";
            this.R3TextBox.Size = new System.Drawing.Size(43, 20);
            this.R3TextBox.TabIndex = 7;
            this.R3TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(163, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "R3";
            // 
            // R2TextBox
            // 
            this.R2TextBox.Enabled = false;
            this.R2TextBox.Location = new System.Drawing.Point(104, 36);
            this.R2TextBox.Name = "R2TextBox";
            this.R2TextBox.Size = new System.Drawing.Size(43, 20);
            this.R2TextBox.TabIndex = 5;
            this.R2TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(114, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "R2";
            // 
            // R1TextBox
            // 
            this.R1TextBox.Enabled = false;
            this.R1TextBox.Location = new System.Drawing.Point(55, 36);
            this.R1TextBox.Name = "R1TextBox";
            this.R1TextBox.Size = new System.Drawing.Size(43, 20);
            this.R1TextBox.TabIndex = 3;
            this.R1TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(65, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "R1";
            // 
            // R0TextBox
            // 
            this.R0TextBox.Enabled = false;
            this.R0TextBox.Location = new System.Drawing.Point(6, 36);
            this.R0TextBox.Name = "R0TextBox";
            this.R0TextBox.Size = new System.Drawing.Size(43, 20);
            this.R0TextBox.TabIndex = 1;
            this.R0TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "R0";
            // 
            // Address_Code_Dec
            // 
            this.Address_Code_Dec.Text = "Address (DEC)";
            this.Address_Code_Dec.Width = 49;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 382);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.listView2);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(800, 420);
            this.Name = "Form1";
            this.Text = "VCPU-16 Emulator";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader Address;
        private System.Windows.Forms.ColumnHeader Hexadecimal;
        private System.Windows.Forms.ColumnHeader Decimal;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newCPUToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveCPUStateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadCPUStateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ColumnHeader Address_Code_Hex;
        private System.Windows.Forms.ColumnHeader Code;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox R0TextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox CRTextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox DSTextBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox CSTextBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox IPTextBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox R15TextBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox R14TextBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox R13TextBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox R12TextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox R11TextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox R10TextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox R9TextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox R8TextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox R7TextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox R6TextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox R5TextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox R4TextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox R3TextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox R2TextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox R1TextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ColumnHeader Address_Code_Dec;
    }
}

