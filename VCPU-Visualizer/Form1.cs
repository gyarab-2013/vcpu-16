﻿using System;
using System.Threading;
using System.Windows.Forms;
using VCPU_16;

namespace VCPU_Visualizer
{
    public partial class Form1 : Form
    {
        private readonly VCPU _cpu;
        private ushort _skip;

        public Form1()
        {
            InitializeComponent();

            _cpu = new VCPU(512) {OnMemorySynchronization = () => listView1.Invoke((MethodInvoker) SynchronizeMemory)};

            var output = VASMCompiler.Compile(@"mov R0, 0;
mov R1, 1;
add R0, R1;
add R1, R0;
brk;
cmp R0, 46368;
je #20;
jmp #4;
hlt;");

            for (ushort i = 0; i < output.Length; i++)
            {
                _cpu.SetMemory(i, output[i]);
            }

            _cpu.Run();

            listView1.DoubleBuffer();
            listView2.DoubleBuffer();

            listView1.ColumnWidthChanging += listView1_ColumnWidthChanging;

            listView1.Columns[0].Width = -2;
            listView1.Columns[1].Width = -2;
            listView1.Columns[2].Width = -2;
        }


        private void listView1_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = -2;
        }

        private void SynchronizeMemory()
        {
            R0TextBox.Text = _cpu.Registers[(int) RegisterType.R0].ToString();
            R1TextBox.Text = _cpu.Registers[(int) RegisterType.R1].ToString();
            R2TextBox.Text = _cpu.Registers[(int) RegisterType.R2].ToString();
            R3TextBox.Text = _cpu.Registers[(int) RegisterType.R3].ToString();
            R4TextBox.Text = _cpu.Registers[(int) RegisterType.R4].ToString();
            R5TextBox.Text = _cpu.Registers[(int) RegisterType.R5].ToString();
            R6TextBox.Text = _cpu.Registers[(int) RegisterType.R6].ToString();
            R7TextBox.Text = _cpu.Registers[(int) RegisterType.R7].ToString();
            R8TextBox.Text = _cpu.Registers[(int) RegisterType.R8].ToString();
            R9TextBox.Text = _cpu.Registers[(int) RegisterType.R9].ToString();
            R10TextBox.Text = _cpu.Registers[(int) RegisterType.R10].ToString();
            R11TextBox.Text = _cpu.Registers[(int) RegisterType.R11].ToString();
            R12TextBox.Text = _cpu.Registers[(int) RegisterType.R12].ToString();
            R13TextBox.Text = _cpu.Registers[(int) RegisterType.R13].ToString();
            R14TextBox.Text = _cpu.Registers[(int) RegisterType.R14].ToString();
            R15TextBox.Text = _cpu.Registers[(int) RegisterType.R15].ToString();
            IPTextBox.Text = _cpu.Registers[(int) RegisterType.IP].ToString();
            CSTextBox.Text = _cpu.Registers[(int) RegisterType.CS].ToString();
            DSTextBox.Text = _cpu.Registers[(int) RegisterType.DS].ToString();
            CRTextBox.Text = _cpu.Registers[(int)RegisterType.CR].ToString();

            Console.WriteLine(R0TextBox.Text);
            Console.WriteLine(R1TextBox.Text);
            Console.WriteLine("-------------");

            listView1.Items.Clear();
            listView2.Items.Clear();

            for (ushort i = 0; i < _cpu.MemorySize; i++)
            {
                var memoryCell = _cpu.GetMemory(i);
                listView1.Items.Add(
                    new ListViewItem(new[] {i.ToString("X4"), memoryCell.ToString("X4"), memoryCell.ToString("D5")}));

                if (_skip != 0)
                {
                    _skip--;
                    continue;
                }

                string decompiled = VASMDecompiler.DecompileOpcode(_cpu, i, out _skip);

                if (decompiled != "Invalid opcode" & decompiled != "NOP")
                {
                    listView2.Items.Add(
                        new ListViewItem(new[] {i.ToString("X4"), i.ToString("D4"), decompiled}));
                }
            }

            Update();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}